# scansrv

Small python project stitched with a hot needle based on Flask.

Uses a ESCL scanning library to offer a web frontend to scan images and upload them to Nextcloud.

## Configuration

The following environment variables are used to configure the application:

```
SCANSRV_OC_USER="scansrv" # Nextcloud service user
SCANSRV_OC_PW="changeit" # password for the Nextcloud service user
SCANSRV_OC_URL="https://nextcloud.example.com" # URL of the Nextcloud instance
SCANSRV_IP="127.0.0.1" # IP of the scanner offering ESCL support
```