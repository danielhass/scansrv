function initialize(){
    $( "#scan" ).click(function() {
        $("#spin-scan").css("visibility", "visible");
        $("#btn-download").css("visibility", "hidden");
        $("#spin-scan").addClass("fa-spinner fa-pulse fa-3x fa-fw");
        $("#spin-scan").removeClass("fa-check-circle");
        $.ajax({
            url: "/api/v1/scan"
        })
        .done(function(){
            console.log("scan complete!")
            $("#spin-scan").removeClass("fa-spinner fa-pulse fa-3x fa-fw");
            $("#btn-download").css("visibility", "visible");
            $("#spin-scan").addClass("fa-check-circle");
        })
        .fail(function(){
            console.log("scan failed!")
            $("#spin-scan").removeClass("fa-spinner fa-pulse fa-3x fa-fw");
            $("#spin-scan").addClass("fa-times-circle ");
        });
    });
}

$( document ).ready(function() {
    initialize();
});