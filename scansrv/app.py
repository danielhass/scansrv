import subprocess
import os
from flask import Flask, render_template, send_file
from escl_scanner.escl_scanner import ESCLScanner

app = Flask(__name__)
FILE_PATH = '/tmp/document.jpg'
DEFAULT_SCAN_IP = "127.0.0.1"
DEFAULT_SCAN_PORT = "80"
DEFAULT_DST_FOLDER = "scansrv"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/api/v1/scan')
def save_scan():
    scanner = ESCLScanner(os.getenv('SCANSRV_IP', DEFAULT_SCAN_IP), os.getenv('SCANSRV_PORT', DEFAULT_SCAN_PORT))
    scanner.scan(FILE_PATH)
    subprocess.call(['curl -u ' + os.environ['SCANSRV_OC_USER'] + ':' + os.environ['SCANSRV_OC_PW'] + ' -T /tmp/document.jpg \"' + os.environ['SCANSRV_OC_URL'] + '/remote.php/dav/files/' + os.environ['SCANSRV_OC_USER'] + '/' + os.getenv('SCANSRV_DST_FOLDER', DEFAULT_DST_FOLDER) + '/' + '$(date \'+%Y-%m-%d-%H:%M:%S\').jpg\"'], shell=True)
    return "OK", 200

@app.route('/api/v1/last_scan', methods = ['GET'])
def last_scan():
    return send_file(FILE_PATH, mimetype='image/jpeg', cache_timeout=0)

if __name__ == "__main__":
    app.run()
