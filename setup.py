import setuptools

setuptools.setup(
    name="scansrv",
    version="0.3.0",
    description="A small Flask app that scans documents and uploads them to Nextcloud",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    package_data = {
        '': [
            'static/*.ico',
            'static/*.js',
            'static/*.css',
            'templates/*.html'
            ],
    },
    install_requires=[
        "uwsgi==2.0.21",
        "flask==2.2.2",
        "escl-scanner @ git+https://gitlab.com/danielhass/python-escl-scanner.git@v0.0.4"
    ]
)
